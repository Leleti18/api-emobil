'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')
const Env = use('Env')

class Image extends Model {
  static get computed(){
    return ['url']
  }
  getUrl({path}) {
    return `${Env.get('APP_URL')}/images/${path}`
    }

    async show ({ params, request, response, view }) {
      const property = await Property.findOrFail(params.id)

      await property.load('images')

      return property
    }

}


module.exports = Image
